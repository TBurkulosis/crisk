﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RiskAPI.Models;

namespace RiskAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RiskController : ControllerBase
    {
        private readonly RiskItemContext _context;

        public RiskController(RiskItemContext context)
        {
            _context = context;
        }

        // GET: api/Risk
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RiskItem>>> GetRisk()
        {
            var risks =  await _context.Risk.ToListAsync();
            if (risks == null)
            {
                return NotFound();
            }
            return risks;
        }

        // GET: api/Risk/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RiskItem>> GetRiskItem(int id)
        {
            var riskItem = await _context.Risk.FindAsync(id);

            if (riskItem == null)
            {
                return NotFound();
            }

            return riskItem;
        }

        // PUT: api/Risk/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRiskItem(int id, RiskItem riskItem)
        {
            if (id != riskItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(riskItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RiskItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Risk
        [HttpPost]
        public async Task<ActionResult<RiskItem>> PostRiskItem(RiskItem riskItem)
        {
            _context.Risk.Add(riskItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRiskItem", new { id = riskItem.Id }, riskItem);
        }

        // DELETE: api/Risk/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RiskItem>> DeleteRiskItem(int id)
        {
            var riskItem = await _context.Risk.FindAsync(id);
            if (riskItem == null)
            {
                return NotFound();
            }

            _context.Risk.Remove(riskItem);
            await _context.SaveChangesAsync();

            return riskItem;
        }

        private bool RiskItemExists(int id)
        {
            return _context.Risk.Any(e => e.Id == id);
        }
    }
}
