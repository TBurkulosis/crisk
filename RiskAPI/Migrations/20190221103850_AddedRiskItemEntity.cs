﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RiskAPI.Migrations
{
    public partial class AddedRiskItemEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RiskList",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Location = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    OwnerId = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    NextReviewDate = table.Column<DateTime>(nullable: false),
                    ReviewInterval = table.Column<int>(nullable: false),
                    Score = table.Column<double>(nullable: false),
                    StrategicObjective = table.Column<string>(nullable: true),
                    CurrentControl = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RiskList", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReviewItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    ReviewDate = table.Column<DateTime>(nullable: false),
                    ReviewScore = table.Column<double>(nullable: false),
                    ReviewInterval = table.Column<int>(nullable: false),
                    ReviewerId = table.Column<int>(nullable: false),
                    Control = table.Column<int>(nullable: false),
                    RiskItemId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReviewItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReviewItem_RiskList_RiskItemId",
                        column: x => x.RiskItemId,
                        principalTable: "RiskList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ReviewItem_RiskItemId",
                table: "ReviewItem",
                column: "RiskItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReviewItem");

            migrationBuilder.DropTable(
                name: "RiskList");
        }
    }
}
