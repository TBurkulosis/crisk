﻿const uri = "api/risk";
let risks = null;
function getCount(data) {
    const el = $("#counter");
    let name = "risk";
    if (data) {
        if (data > 1) {
            name = "risks";
        }
        el.text(data + " " + name);
    } else {
        el.text("No " + name);
    }
}

$(document).ready(function () {
    getData();
});

function getData() {
    $.ajax({
        type: "GET",
        url: uri,
        cache: false,
        success: function (data) {
            const tbody = $("#risks");

            $(tbody).empty();

            getCount(data.length);

            $.each(data, function (_key, item) {
                const tr = $("<tr></tr>")
                    .append($("<td></td>").text(item.id))
                    .append($("<td></td>").text(item.name))
                    .append($("<td></td>").text(item.description))
                    .append($("<td></td>").text(item.location))
                    .append($("<td></td>").text(item.ownerid))
                    .append($("<td></td>").text(item.status))
                    .append($("<td></td>").text(Date(item.createddate)))
                    .append($("<td></td>").text(Date(item.nextreviewdate)))
                    .append($("<td></td>").text(item.reviewinterval))
                    .append($("<td></td>").text(item.pastreviews))
                    .append($("<td></td>").text(item.score.toFixed(4)))
                    .append($("<td></td>").text(item.strategicobjective))
                    .append($("<td></td>").text(item.currentcontrol))
                    .append($("<button class=\"editButton\">Edit</button>").on("click", function () {
                        editItem(item.id);
                    })
                    ).append(
                        $("<td></td>").append($("<button>Delete</button>").on("click", function () {
                            deleteItem(item.id);
                        })
                    ));
                tr.appendTo(tbody);
            });

            risks = data;
        }
    })
}


function addItem() {
    const item = {
        name: $("#add-name").val(),
        location: $("#add-location").val(),
        description: $("#add-description").val(),
        ownerid: $("#add-ownerid").val(),
        status: $("#add-status").val(),
        createdDate: new DateTime(),
        nextreviewdate: new DateTime(new Date().getDate() + this.reviewinterval),
        reviewinterval: $("#add-reviewinterval").val(),
        pastreviews: $("#add-pastreviews").val(),
        score: $("#add-score").val(),
        strategicobjective: $("#add-strategicobjective").val(),
        currentcontrol: $("#add-currentcontrol").val()
    };

    $.ajax({
        type: "POST",
        accept: "application/json",
        url: uri,
        contentType: "application/json",
        data: JSON.stringify(item),
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong!");
        },
        success: function (result) {
            getData();
            $("#add-name").val("");
            $("#add-location").val("");
            $("#add-description").val("");
            $("#add-ownerid").val("");
            $("#add-status").val("");
            $("#add-reviewinterval").val("");
            $("#add-pastreviews").val("");
            $("#add-score").val("");
            $("#add-strategicobjective").val("");
            $("#add-currentcontrol").val("");
        }
    })
}

function deleteItem(id) {
    $.ajax({
        url: uri + "/" + id,
        type: "DELETE",
        success: function (result) {
            getData();
        }
    });
}

function editItem(id) {
    $.each(risks, function (key, item) {
        if (item.id === id) {
            $("#add-name").val(item.name);
            $("#add-location").val(item.location);
            $("#add-description").val(item.description);
            $("#add-ownerid").val(item.ownerid);
            $("#add-status").val(item.status);
            $("#add-reviewinterval").val(item.reviewinterval);
            $("#add-score").val(item.score);
            $("#add-strategicobjective").val(item.strategicobjective);
            $("#add-currentcontrol").val(item.currentcontrol);
        }
    });
    $("#spoiler").css({ display: "block" });
}

$(".my-form").on("submit", function () {
    const item = {
        name: $("#add-name").val(),
        location: $("#add-location").val(),
        description: $("#add-description").val(),
        ownerid: $("#add-ownerid").val(),
        status: $("#add-status").val(),
        createdDate: new DateTime(),
        nextreviewdate: new DateTime(new Date().getDate() + this.reviewinterval),
        reviewinterval: $("#add-reviewinterval").val(),
        pastreviews: $("#add-pastreviews").val(),
        score: $("#add-score").val(),
        strategicobjective: $("#add-strategicobjective").val(),
        currentcontrol: $("#add-currentcontrol").val()
    };

    $.ajax({
        url: uri + "/" + $("#edit-id").val(),
        type: "PUT",
        accepts: "application/json",
        contentType: "application/json",
        data: JSON.stringify(item),
        success: function (result) {
            getData();
        }
    });

    closeInput();
    return false;
});

function closeInput() {
    $("#spoiler").css("display: none");
}