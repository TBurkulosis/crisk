DROP TABLE risk;
DROP TABLE review;

CREATE TABLE risk (
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    location VARCHAR(50) NOT NULL,
    description VARCHAR(100) ,
    ownerid INT NOT NULL,
    status VARCHAR(20) NOT NULL,
    createddate DATE NOT NULL DEFAULT GETDATE(),
    reviewinterval INT,
    nextreviewdate DATE,
    pastreviews NVARCHAR(2000),
    score FLOAT(7),
    strategicobjective VARCHAR(40),
    currentcontrol INT
);

CREATE TABLE review (
    id INT NOT NULL PRIMARY KEY,
    reviewdate TIMESTAMP NOT NULL,
    reviewscore FLOAT(7),
    reviewinterval INT,
    reviewerid INT NOT NULL,
    control INT NOT NULL
);



INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000001, 'risk1', 'UK', 'A risk in the UK', 0, 'Initialised', 30, NULL, 0.5762, 'Security enhancement', 1);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000002, 'risk2', 'AU', 'A risk in Australia', 0, 'In Progress', 7, NULL, 0.7641, 'OH&S', 2);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000003, 'risk3', 'CA', 'A risk in Canada', 0, 'Paused', 2, NULL, 0.2235, 'Asset Protection', 1);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000004, 'risk4', 'SL', 'A risk in Sri Lanka', 0, 'Complete', 4, NULL, 0.2344, 'Asset Protection', 1);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000005, 'risk5', 'AU', 'A risk in Australia', 0, 'Complete', 14, NULL, 0.4356, 'OH&S', 2);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000006, 'risk6', 'AU', 'A risk in Australia', 0, 'In Progress', 60, NULL, 0.9756, 'OH&S', 4);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000007, 'risk7', 'CA', 'A risk in Canada', 0, 'Initialised', 120, NULL, 0.2237, 'Security enhancement', 3);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000008, 'risk8', 'UK', 'A risk in the UK', 0, 'In Progress', 45, NULL, 0.5762, 'Asset Protection', 2);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000009, 'risk9', 'CA', 'A risk in Canada', 0, 'In Progress', 90, NULL, 0.8745, 'OH&S', 1);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000010, 'risk10', 'AU', 'A risk in Australia', 0, 'Paused', 6, NULL, 0.9832, 'OH&S', 1);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000011, 'risk11', 'AU', 'A risk in Australia', 0, 'In Progress', 2, NULL, 0.9956, 'OH&S', 1);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000012, 'risk12', 'SL', 'A risk in Sri Lanka', 0, 'In Progress', 1, NULL, 0.7613, 'Asset Protection', 2);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000013, 'risk13', 'CA', 'A risk in Canada', 0, 'Initialised', 12, NULL, 0.6658, 'OH&S', 1);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000014, 'risk14', 'AU', 'A risk in Australia', 0, 'In Progress', 10, NULL, 0.8956, 'Security enhancement', 1);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000015, 'risk15', 'AU', 'A risk in Australia', 0, 'In Progress', 30, NULL, 0.6784, 'Asset Protection', 1);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000016, 'risk16', 'UK', 'A risk in the UK', 0, 'Initialised', 6, NULL, 0.9568, 'OH&S', 12);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000017, 'risk17', 'AU', 'A risk in Australia', 0, 'Paused', 7, NULL, 0.8888, 'OH&S', 13);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000018, 'risk18', 'CA', 'A risk in Canada', 0, 'In Progress', 30, NULL, 0.9999, 'Security enhancement', 11);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000019, 'risk19', 'AU', 'A risk in Australia', 0, 'Initialised', 14, NULL, 0.7777, 'Asset Protection', 1);
INSERT INTO risk (id, name, location, description, ownerid, status, reviewinterval, pastreviews, score, strategicobjective, currentcontrol) VALUES (000020, 'risk20', 'SL', 'A risk in Sri Lanka', 0, 'Initialised', 30, NULL, 0.5000, 'OH&S', 11);


CREATE TABLE review (
    id INT(6) NOT NULL PRIMARY KEY,
    reviewdate DATE NOT NULL DEFAULT GETDATE(),
    reviewscore FLOAT(5,4),
    reviewerid INT(6) NOT NULL,
    control INT(6) NOT NULL
);

INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000001, 0.3945, 0, 11);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000002, 0.8612, 0, 1);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000003, 0.6237, 0, 1);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000004, 0.7695, 0, 2);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000005, 0.9565, 1, 2);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000006, 0.9467, 1, 1);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000007, 0.7915, 1, 2);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000008, 0.8882, 1, 12);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000009, 0.5656, 1, 12);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000010, 0.2371, 1, 13);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000011, 0.9526, 0, 13);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000012, 0.3362, 0, 13);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000013, 0.6495, 0, 3);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000014, 0.7748, 0, 4);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000015, 0.3362, 0, 3);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000016, 0.1135, 0, 1);
INSERT INTO review (id, reviewscore, reviewerid, control) VALUES (000017, 0.6425, 0, 1);

