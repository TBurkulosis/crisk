﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiskAPI.Models
{
    public class RiskItemContext : DbContext
    {
        public RiskItemContext(DbContextOptions<RiskItemContext> options)
            : base(options)
        {
        }
        public DbSet<RiskItem> Risk { get; set; }
    }
}
