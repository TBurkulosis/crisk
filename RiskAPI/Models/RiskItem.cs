﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RiskAPI.Models
{
    public class RiskItem
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public int OwnerId { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime NextReviewDate { get; set; }
        public int ReviewInterval { get; set; }
        public IEnumerable<ReviewItem> PastReviews { get; set; }
        [Range(0.0, 1.0)]
        public double Score { get; set; }
        public string StrategicObjective { get; set; }
        public int CurrentControl { get; set; }
    }
}
