﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiskAPI.Models
{
    public class ReviewItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime ReviewDate { get; set; }
        public double ReviewScore { get; set; }
        public int ReviewInterval { get; set; }
        public int ReviewerId { get; set; }
        public int Control { get; set; }
    }
}
